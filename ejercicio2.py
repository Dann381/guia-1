print("Ingrese la cantidad de trajes a evaluar (al menos 3):")
trajes = int(input())
while trajes < 3:
    print("Cantidad de trajes incorrecta, por favor ingrese un número mayor o igual a 3")
    trajes = int(input())
print("La cantidad de trajes es:",trajes)
totalprecio = 0
for i in range(trajes):
    precio = 0
    print("Ingrese el valor traje número",(i+1))
    valor = int(input())
    if valor > 10000:
        precio = valor - (valor * 0.15)
        print("El valor del traje",(i+1),"es de",precio,"pesos")
        totalprecio = totalprecio + precio
    else:
        precio = valor - (valor * 0.08)
        print("El valor del traje",(i+1),"es de",precio,"pesos")
        totalprecio = totalprecio + precio
print("El precio total a pagar es de",totalprecio,"pesos")
