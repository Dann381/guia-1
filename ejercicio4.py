print("Ingrese la cantidad de alumnos")
N = int(input())
edadtotal = 0
for i in range(N):
    print("Ingrese la edad del alumno número",(i+1),"(entre 0 y 100)")
    edad = int(input())
    while edad > 100 or edad < 0:
        print("La edad ingresada no es válida, ingresela nuevamente:")
        edad = int(input())
    edadtotal = edadtotal + edad
promedioedad = edadtotal/N
print("El promedio de las edades es",promedioedad)
