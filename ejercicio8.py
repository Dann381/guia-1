print("Ingrese cuantas personas solicitan el trabajo (entre 1 y 100 personas):")
personas = int(input())
while personas > 100 or personas < 1:
    print("Número ingresado no válido, por favor ingrese un número entre 1 y 100:")
    personas = int(input())
edadtotal = 0
año = 0
for i in range(personas):
    print("<<<<<<<<<<<<<<< Solicitante número",(i+1),">>>>>>>>>>>>>>>")
    print("A continuación ingrese el año en el que nació")
    año = int(input("Año:"))
    edad = (2020 - año)
    print("Edad:",edad,"años")
    edadtotal = edadtotal + edad
promedioedad = (edadtotal/personas)
print("La edad promedio de los solicitantes es",promedioedad,"años")
