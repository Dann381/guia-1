print("Ingrese la cantidad de horas que estuvo estacionado")
hora = int(input("Hora(s):"))
while hora < 0 or hora > 23:
    print("Valor ingresado incorrecto, por favor ingrese un número entre 0 y 24")
    hora = int(input("Hora(s):"))
print("Ingrese la cantidad de minutos")
minuto = int(input("Minuto(s):"))
while minuto < 0 or minuto > 59:
    print("Valor ingresado incorrecto, por favor ingrese un número entre 0 y 59")
    minuto = int(input("Minuto(s):"))
precio = 0
print("Usted estuvo estacionado",hora,"hora(s) y",minuto,"minuto(s)")
if hora == 0 and minuto == 0:
    print("Usted no debe pagar nada")
elif minuto != 0:
    hora = hora + 1
    precio = hora * 500
    print("Usted debe pagar",precio,"pesos")
elif minuto == 0:
    precio = hora * 500
    print("Usted debe pagar",precio,"pesos")
